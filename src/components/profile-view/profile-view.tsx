import {Component, ComponentInterface, h, Prop} from '@stencil/core';

import "@ionic/core";

@Component({
  tag: 'profile-view',
  shadow: true,
})
export class ProfileView implements ComponentInterface {

  @Prop() profile: { uri: string, name: string, location: string, role: string, imageSrc: string, note: string, homepage: string };

  render() {
    const {name, location, role, imageSrc, homepage, note} = this.profile;
    return  <ion-card>
      <ion-card-header>
        <ion-avatar>
          <img src={imageSrc}/>
        </ion-avatar>
        <ion-card-title>{name}</ion-card-title>
        <ion-card-subtitle>{role}</ion-card-subtitle>
      </ion-card-header>
      <ion-card-content>
        <ion-grid>
          <ion-row>
            <ion-col size="12" size-sm>
              <ion-icon name="location-outline"></ion-icon> {location}
            </ion-col>
            <ion-col size="12" size-sm>
              <ion-icon name="globe-outline"></ion-icon> <a href={homepage}>{homepage}</a>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size="12" size-sm>
              {note}
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-card-content>
    </ion-card>






  }

}
