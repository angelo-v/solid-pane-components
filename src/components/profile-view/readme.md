# profile-view



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                                                                                                               | Default     |
| --------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------ | ----------- |
| `profile` | --        |             | `{ uri: string; name: string; location: string; role: string; imageSrc: string; note: string; homepage: string; }` | `undefined` |


## Dependencies

### Depends on

- ion-card
- ion-card-header
- ion-avatar
- ion-card-title
- ion-card-subtitle
- ion-card-content
- ion-grid
- ion-row
- ion-col
- ion-icon

### Graph
```mermaid
graph TD;
  profile-view --> ion-card
  profile-view --> ion-card-header
  profile-view --> ion-avatar
  profile-view --> ion-card-title
  profile-view --> ion-card-subtitle
  profile-view --> ion-card-content
  profile-view --> ion-grid
  profile-view --> ion-row
  profile-view --> ion-col
  profile-view --> ion-icon
  ion-card --> ion-ripple-effect
  style profile-view fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
